/**
 * Created by anth on 9/25/17.
 */

let graph_container;

const graph_api = {
    cy: undefined,
    css: undefined,
    id: 0,

    init: function (container) {
        this.cy = cytoscape({
            container,

            layout: {
                name: 'cose',
                padding: 10
            },

            style: this.css,

            elements: {
                nodes: [],
                edges: []
            },

            selectionType: 'single'
        });
        return this.cy;
    },

    addNode: function addNode(name, shape = 'rectangle', renderedPosition) {
        const node = this.cy.add({
            data: {
                id: this.id,
                name: name,
                faveColor: '#6FB1FC',
                faveShape: shape,
            },
            renderedPosition: renderedPosition
        });

        const popperRef = node.popperRef();
        const tooltip = tippy(popperRef, {
            html: '#editor-template',
            placement: 'bottom',
            trigger: 'manual',
            interactive: true,
            distance: 0
        }).tooltips[0];

        const nameInput = $('input[name="name"]', tooltip.popper);

        nameInput.val(name);
        nameInput.on('keyup keydown', (e) => {
            node.data('name', $(e.target).val())
        });

        node.on('cxttap', () => setTimeout(() => {
            if (!tooltip.state.visible) {
                tooltip.show();
                nameInput.focus();
            }
        }, 0));

        return ++this.id;
    },

    addRelation: function (id1, id2) {
        this.cy.add(this.edgeParams(id1, id2));
    },

    edgeParams: function (id1, id2) {
        return {
            data: {
                source: id1,
                target: id2,
                faveColor: '#6FB1FC',
                strength: 90
            }
        }
    }

};

$.when($.get('/css/style.cycss'), $.ready)
    .then((result) => {
        graph_api.css = result[0];
        graph_container = $('#graph');
        return window.cy = graph_api.init(graph_container);
    })
    .then((cy) => {
        cy.edgehandles({
            toggleOffOnLeave: true,
            preview: true,

            handleNodes: "node",

            handleSize: 10,
            handleHitThreshold: 2,

            stop: function () {
                cy.nodes('.forbidden').removeClass('forbidden');
                setCursorForbidden(false);
            },

            edgeType: function (source, target) {
                //don't check again if was already checked
                if(target.hasClass('forbidden')) {
                    setCursorForbidden(true);
                    return null;
                }

                let result;

                $.ajax({
                    url: '/can_add',
                    data: {
                        o1: source.data(),
                        o2: target.data()
                    },
                    dataType: 'json',
                    async: false, //todo make edgeType async
                    success: function (res) {
                        result = res;
                    }
                });

                if(!result.result) {
                    target.addClass('forbidden');
                    setCursorForbidden(true);
                }

                return result.result ? 'flat': null;
            },

            edgeParams: function (source, target) {
                return graph_api.edgeParams(source.id(), target.id());
            }
        });
    })
    .then(() => {
        //cytoscape doesn't support custom cursor css, need this hack to change cursor type
        cy.on('mouseover', 'node', function (e) {
            const target = e.target;
            setCursorForbidden(target.hasClass('forbidden'));
        });
        cy.on('mouseout', 'node', function (e) {
            setCursorForbidden(false)
        });
    });

function setCursorForbidden(value) {
    if(value)
        $(graph_container).addClass('forbidden');
    else
        $(graph_container).removeClass('forbidden');
}
