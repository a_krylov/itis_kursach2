$(function () {

    //set node type titles
    $('.node-type').each((idx, el) => {
        el = $(el);
        el.attr('title', el.data('name'));
    });

    $('.node-type').draggable({
        helper: 'clone'
    });

    function dropNode(e, ui) {
        const x = ui.offset.left - $(this).offset().left;
        const y = ui.offset.top - $(this).offset().top;

        graph_api.addNode(ui.draggable.data('name'), ui.draggable.data('shape'), {x, y});
    }

    $('#graph').droppable({
        drop: dropNode
    });
});

