/**
 * Created by anth on 9/27/17.
 */

const express = require('express');

const app = express();

app.use(express.static('static'));

app.get('/can_add', function (req, res) {
    const o1 = req.query.o1;
    const o2 = req.query.o2;

    const r = o1.faveShape === o2.faveShape;
    res.send({result: r});
});

app.listen(process.env.PORT || 3000);